<?php

/**
 * 361GRAD Element Partnerteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementPartnerteaser\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\FilesModel;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDsePartnerteaser
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDsePartnerteaser extends ContentElement
{
    /**
     * Template name.
     *
     * @var string The template
     */
    protected $strTemplate = 'ce_dse_partnerteaser';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        $this->Template->addImage = false;

        // Add an image
        if ($this->addImage && $this->singleSRC != '') {
            // Get Files Model of image
            $objModel = FilesModel::findByUuid($this->singleSRC);

            if ($objModel !== null && is_file(TL_ROOT . '/' . $objModel->path)) {
                // Replace singleSRC with image path
                $this->singleSRC = $objModel->path;
                // Add image data into template
                $this->addImageToTemplate($this->Template, $this->arrData);
            }
        }

        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';
    }

    /**
     * Creates special label for checkboxes with a colored style
     *
     * @param string $bgc   The Background Color.
     * @param string $fgc   The Foreground Color.
     * @param string $title The Label Title.
     *
     * @return string
     */
    public static function refColor($bgc, $fgc, $title)
    {
        $style = 'display:inline-block;width:100px;text-align:center;';
        return '<span style="background-color:' . $bgc . ';color:' . $fgc . ';' . $style . '">' . $title . '</span>';
    }
}
