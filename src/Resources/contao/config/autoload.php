<?php

/**
 * 361GRAD Element Partnerteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Templates
TemplateLoader::addFiles(
    [
        'ce_dse_partnerteaser'      => 'vendor/361grad-elements/dse-element-partnerteaser/src/Resources/contao/templates'
    ]
);
