<?php

/**
 * 361GRAD Element Partnerteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_partnerteaser'] =
    'Dse\\ElementsBundle\\ElementPartnerteaser\\Element\\ContentDsePartnerteaser';
