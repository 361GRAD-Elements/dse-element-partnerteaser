<?php

/**
 * 361GRAD Element Partnerteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_partnerteaser'] =
    '{type_legend},type;' .
    '{image_legend},addImage;' .
    '{partnertitle_legend},dse_partnername,dse_partnerposition;' .
    '{partnercontacts_legend},dse_partnerphone,dse_partnermail;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_partnername'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_partnername'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_partnerposition'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_partnerposition'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_partnerphone'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_partnerphone'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_partnermail'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_partnermail'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];