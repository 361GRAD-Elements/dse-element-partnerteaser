<?php

/**
 * 361GRAD Element Partnerteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_partnerteaser'] = ['Partner Teaser', 'Partner Teaser'];

$GLOBALS['TL_LANG']['tl_content']['partnertitle_legend']   = 'Name/job settings';
$GLOBALS['TL_LANG']['tl_content']['dse_partnername']  =
    ['Name', 'Here you can add a Name for this element.'];
$GLOBALS['TL_LANG']['tl_content']['dse_partnerposition']  =
    ['Job', 'Here you can add a Job for this element.'];

$GLOBALS['TL_LANG']['tl_content']['partnercontacts_legend']   = 'Phone/email settings';
$GLOBALS['TL_LANG']['tl_content']['dse_partnerphone']  =
    ['Phone', 'Here you can add a Phone for this element.'];
$GLOBALS['TL_LANG']['tl_content']['dse_partnermail']  =
    ['Email', 'Here you can add an Email for this element.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];