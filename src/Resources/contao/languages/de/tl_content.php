<?php

/**
 * 361GRAD Element Partnerteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_partnerteaser'] = ['Partner Teaser', 'Partner Teaser'];

$GLOBALS['TL_LANG']['tl_content']['partnertitle_legend']   = 'Name / Job-Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_partnername']  =
    ['Name', 'Hier können Sie einen Namen für dieses Element hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_partnerposition']  =
    ['Job', 'Hier können Sie einen Job für dieses Element hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['partnercontacts_legend']   = 'Telefon- / E-Mail-Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_partnerphone']  =
    ['Telefon', 'Hier können Sie ein Telefon für dieses Element hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_partnermail']  =
    ['E-mail', 'Hier können Sie eine E-Mail für dieses Element hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];